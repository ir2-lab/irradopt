function fig_1(x, y, z, u, dep, w, ymean) %% x -> x-axis data , y-> y-axis data , z-> normalisation exponent

clrs = [0   0.4470   0.7410;
   0.8500   0.3250   0.0980;
   0.9290   0.6940   0.1250;
   0.4940   0.1840   0.5560;
   0.4660   0.6740   0.1880;
   0.3010   0.7450   0.9330;
   0.6350   0.0780   0.1840];

q = -log10(z);
exp=floor(log10(ymean));
pow=10^exp;

plot(x/10000, y*z, 'color', clrs(1,:), 'LineWidth', 1.5)
hold on
ylim([0 u(2)])
xlim([0 u(1)])

plot([dep dep]/10000, ylim, '--', 'color', 'k')

hold off
xlabel('Depth (\mum)')
ylabel ({['\sigma_i (10^{', num2str(q), '}cm^2)']})

str = [' Mean \sigma_i = '  num2str(ymean/pow,'%.2d'), ' 10^{', num2str(exp),'}'];
t=text(dep/20000 , 0.5, str,'edgecolor', clrs(2,:),...
  'VerticalAlignment','middle',...
  'HorizontalAlignment','center',...
  'fontsize', 8);

title('Implantation cross-section')
h=text(dep/10/10000,0.9*u(2),'5.5 MeV Fe on Fe','edgecolor', 'k');
print2png(gcf,[12 9],'Fig1')
