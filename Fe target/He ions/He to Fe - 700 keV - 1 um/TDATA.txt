====== He (200) into Layer 1                    =======
        SRIM-2013.00
=======================================
      Data  of  TRIM  Calculation      
=======================================
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
Ion = He ( 2)   Ion Mass= 004.0030
Energy  = 7200000.E-04 keV
Ion Angle to Surface = 0 degrees
============= TARGET MATERIAL ======================================
  Layer # 1 - Layer 1
  Layer # 1 - Bottom Depth=     2.E+04 A
  Layer # 1- Density = 8.481E22 atoms/cm3 = 7.865 g/cm3
  Layer # 1- Fe = 100  Atomic Percent = 100  Mass Percent
====================================================================
Target energies for target atom = Fe
    Displacement = 40 eV, Binding = 3 eV, Surface = 4.34 eV
====================================================================
Depth Range of Tabulated Data=  000000.E+00  -  175000.E-01  Angstroms
====================================================================
Total Ions calculated =  009999
Average Range         =   12387.E+00 Angstroms
Average Straggling    =   12222.E-01 Angstroms
Average Vacancy/Ion   =   71634.E-03
====================================================================
Total Backscattered Ions=  0 
Total Transmitted Ions  =  0 
====================================================================
See files SRIM Outputs\*.txt for tables :
   TDATA.txt    -- Details of the calculation.
   RANGE.txt    -- Final distribution of Ions/Recoils
   VACANCY.txt  -- Distribution of Vacancies
   IONIZ.txt    -- Distribution of Ionization
   PHONON.txt   -- Distribution of Phonons
   E2RECOIL.txt -- Energy transferred to recoils
   NOVAC.txt    -- Replacement collisions
   LATERAL.txt  -- Lateral Spread of Ions
