function fig_4(x, y, u,dep, width, ymean) %% x -> x-axis data , y-> y-axis data
                                          %%u here is 1x3, u=[xupl, ylowl, yupl]
clrs = [0   0.4470   0.7410;
   0.8500   0.3250   0.0980;
   0.9290   0.6940   0.1250;
   0.4940   0.1840   0.5560;
   0.4660   0.6740   0.1880;
   0.3010   0.7450   0.9330;
   0.6350   0.0780   0.1840];

exp=floor(log10(ymean));
pow=10^exp;

semilogy(width/10000,ymean*ones(size(width)), '--','color',clrs(2,:),'linewidth', 1.5)
ylim([u(2) u(3)])
xlim([0 u(1)])
hold on
semilogy (x/10000, y, 'color', clrs(1,:), 'LineWidth', 1.5)

plot([dep dep]/10000, ylim, '--', 'color','k')
hold off


str = [' Mean \sigma_i/\sigma_d = ', num2str(ymean/pow,'%.2d'), ' 10^{', num2str(exp),'}'];
t=text(1.08*dep/10000 , ymean, str,'edgecolor', clrs(2,:),...
  'VerticalAlignment','middle',...
  'HorizontalAlignment','left',...
  'fontsize', 8);
ylabel ({['\sigma_i/\sigma_d']})
xlabel('Depth (\mum)')
title('Implantations per dpa')
h=text(dep/10/10000,0.6*u(3),'1 MeV Li on Fe','edgecolor', 'k');
print2png(gcf,[12 9],'Fig4')
