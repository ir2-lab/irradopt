====== Li (10) into Layer 1                     =======
        SRIM-2013.00
=======================================
      Data  of  TRIM  Calculation      
=======================================
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
Ion = Li ( 3)   Ion Mass= 007.0160
Energy  = 4750000.E-04 keV
Ion Angle to Surface = 0 degrees
============= TARGET MATERIAL ======================================
  Layer # 1 - Layer 1
  Layer # 1 - Bottom Depth=     1.E+04 A
  Layer # 1- Density = 8.481E22 atoms/cm3 = 7.865 g/cm3
  Layer # 1- Fe = 100  Atomic Percent = 100  Mass Percent
====================================================================
Target energies for target atom = Fe
    Displacement = 40 eV, Binding = 3 eV, Surface = 4.34 eV
====================================================================
Depth Range of Tabulated Data=  000000.E+00  -  129060.E-01  Angstroms
====================================================================
Total Ions calculated =  009999
Average Range         =   81794.E-01 Angstroms
Average Straggling    =   13984.E-01 Angstroms
Average Vacancy/Ion   =   19679.E-02
====================================================================
Total Backscattered Ions=  15 
Total Transmitted Ions  =  0 
====================================================================
See files SRIM Outputs\*.txt for tables :
   TDATA.txt    -- Details of the calculation.
   RANGE.txt    -- Final distribution of Ions/Recoils
   VACANCY.txt  -- Distribution of Vacancies
   IONIZ.txt    -- Distribution of Ionization
   PHONON.txt   -- Distribution of Phonons
   E2RECOIL.txt -- Energy transferred to recoils
   NOVAC.txt    -- Replacement collisions
   LATERAL.txt  -- Lateral Spread of Ions
