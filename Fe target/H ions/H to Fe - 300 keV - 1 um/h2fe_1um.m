clear all

N = 8.481*10^22;  %%atomic density se cm-3
data = srim2mat('.');
%function out = srim2mat('Fe to Fe_ 8MeV_1micron')
% The structure out has the following fields:
%   x   :  target depth (Angstroem)
%   Vi  :  vacancies from ions (Atoms/ion-Angstroem)
%   Vr  :  vacancies from recoils (Atoms/ion-Angstroem)
%   RC  :  replacement collisions (Atoms/ion-Angstroem)
%   EIi :  ionization energy by ions (eV/ion-Angstroem)
%   EIr :  ionization energy by target recoils (eV/ion-Angstroem)
%   EPi :  phonon energy by ions (eV/ion-Angstroem)
%   EPr :  phonon energy by target recoils (eV/ion-Angstroem)
%   ERi :  recoil energy from ions (eV/ion-Angstroem)
%   ERr :  recoil energy absorbed by target recoils (eV/ion-Angstroem)
%   Ri  :  range distribution of ions (atoms/cm3) / (atoms/cm2)
%   Rr  :  range distribution of recoils (atoms/cm3) / (atoms/cm2)
%   parsedFiles : cell array with all succesfully parsed files
%   failedFiles : cell array with all failed files

n_ions = 9999;                                            % number of ions

dep = 10000;                                              %real depth in Angstroem
erec = 1e8*data.ERi/N;                                    % eV cm energy to recoil per atom per unit flux
sigma_pka= 1e8*(data.Vi)/N;                               % pka displacements per atom per unit flux
sigmad = 1e8*(data.Vi+data.Vr)/N;                         % cm2 , total vacancies per atom per unit flux / damage cross section �(xi)
i = find(data.x <= dep);

sigma_mean = mean(sigmad(i))                              %se cm2  mean damage cross section
dsigma = std(sigmad(i))/sqrt(n_ions)                      %standard deviation of damage cross section

erec_mean = mean(erec(i))                                 %mean energy to recoil per atom per unit flux
erec_pka = mean(erec(i)./sigma_pka(i))                    %recoil energy per PKA
derec_pka = std(erec(i)./sigma_pka(i))/sqrt(n_ions)       %standard deviatonn of mean recoil energy per PKA

disp_pka = mean(sigmad(i)./sigma_pka(i))                  %mean displacemtns per PKA
d_disp_pka = std(sigmad(i)./sigma_pka(i))/sqrt(n_ions)    %standard deviation of displacemtns per PKA

imp_mean = mean (data.Ri(i)/N./sigmad(i))                 %mean number of implantations per pka
d_imp = std(data.Ri(i)/N./sigmad(i))/sqrt(n_ions)         %standard deviation of displacements per PKA
width = data.x(i);                                        %real depth vector

ion_mean = mean(data.Ri(i)/N)                             %mean implantation cross section


figure 1
clf
fig_1(data.x, data.Ri/N,1e19,[2 8],dep,width,ion_mean)

figure 2
clf
fig_2(data.x, sigmad,1e18, [2 2.5],dep,width,sigma_mean)



figure 3
clf
fig_3(data.x, erec, 1e16,[2 2.5], dep)


figure 4
clf
fig_4(data.x, data.Ri/N./sigmad,[2 1e-3 1e1], dep, width, imp_mean)


figure 5
clf
fig_5(data.x, erec./sigma_pka,[2 3.5],dep, width, mean(erec(i)./sigma_pka(i)))


figure 6
clf
fig_6(data.x, sigmad./sigma_pka,[2 5.5], dep, width ,mean(sigmad(i)./sigma_pka(i)))


