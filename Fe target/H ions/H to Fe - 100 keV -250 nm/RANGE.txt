====== H (100) into Layer 1                     =======
        SRIM-2013.00
==================================================
      ION and final RECOIL ATOM Distributions     
 See  SRIM Outputs\TDATA.txt for calculation details
==================================================
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
----------------------------------------------------------------
  There are NO RECOILS unless a full TRIM calculation is made.   
----------------------------------------------------------------
See file :  SRIM Outputs\TDATA.txt   for details of calculation
 Ion    =  H   Energy = 100  keV
============= TARGET MATERIAL ======================================
Layer  1 : Layer 1
Layer Width =     8.E+03 A   Layer # 1- Density = 8.481E22 atoms/cm3 = 7.865 g/cm3
  Layer # 1- Fe = 100  Atomic Percent = 100  Mass Percent
====================================================================
 Total Ions calculated =9999.00
 Ion Average Range =   455.0E+01 A   Straggling =   770.2E+00 A
 Ion Lateral Range =   832.8E+00 A   Straggling =   104.5E+01 A
 Ion Radial  Range =   130.0E+01 A   Straggling =   690.1E+00 A
====================================================================
 Transmitted Ions =;  Backscattered Ions =22
 (These are not included in Skewne- and Kurtosis below.)

 Range Skewne- = -001.2993  &= [-(X-Rp)^3]/[N*Straggle^3]
 Range Kurtosis = 006.4090  &= [-(X-Rp)^4]/[N*Straggle^4]
 Statistical definitions above are those used in VLSI implant modelling.
====================================================================
=================================================================
  Table Distribution Units are >>>  (Atoms/cm3) / (Atoms/cm2)  <<<  
=================================================================
   DEPTH          H       Recoil     
   (Ang.)       Ions     Distribution
-----------  ----------  ------------
756900.E-04  0.0000E+00  0.0000E+00
151370.E-03  1.3215E+02  0.0000E+00
227050.E-03  0.0000E+00  0.0000E+00
302730.E-03  0.0000E+00  0.0000E+00
378410.E-03  5.2859E+02  0.0000E+00
454090.E-03  2.6430E+02  0.0000E+00
529770.E-03  1.3215E+02  0.0000E+00
605450.E-03  5.2859E+02  0.0000E+00
681130.E-03  6.6074E+02  0.0000E+00
756810.E-03  6.6074E+02  0.0000E+00
832490.E-03  5.2859E+02  0.0000E+00
908170.E-03  3.9645E+02  0.0000E+00
983850.E-03  6.6074E+02  0.0000E+00
105953.E-02  5.2859E+02  0.0000E+00
113521.E-02  2.6430E+02  0.0000E+00
121089.E-02  3.9645E+02  0.0000E+00
128657.E-02  7.9289E+02  0.0000E+00
136225.E-02  9.2504E+02  0.0000E+00
143793.E-02  1.1893E+03  0.0000E+00
151361.E-02  5.2859E+02  0.0000E+00
158929.E-02  6.6074E+02  0.0000E+00
166497.E-02  6.6074E+02  0.0000E+00
174065.E-02  3.9645E+02  0.0000E+00
181633.E-02  9.2504E+02  0.0000E+00
189201.E-02  1.1893E+03  0.0000E+00
196769.E-02  1.1893E+03  0.0000E+00
204337.E-02  1.0572E+03  0.0000E+00
211905.E-02  1.7179E+03  0.0000E+00
219473.E-02  2.1144E+03  0.0000E+00
227041.E-02  1.7179E+03  0.0000E+00
234609.E-02  1.1893E+03  0.0000E+00
242177.E-02  2.2465E+03  0.0000E+00
249745.E-02  1.8501E+03  0.0000E+00
257313.E-02  3.8323E+03  0.0000E+00
264881.E-02  3.3037E+03  0.0000E+00
272449.E-02  3.4359E+03  0.0000E+00
280017.E-02  3.3037E+03  0.0000E+00
287585.E-02  3.9645E+03  0.0000E+00
295153.E-02  4.3609E+03  0.0000E+00
302721.E-02  5.8145E+03  0.0000E+00
310289.E-02  5.9467E+03  0.0000E+00
317857.E-02  8.0611E+03  0.0000E+00
325425.E-02  6.8717E+03  0.0000E+00
332993.E-02  9.7790E+03  0.0000E+00
340561.E-02  9.3825E+03  0.0000E+00
348129.E-02  1.2951E+04  0.0000E+00
355697.E-02  1.3479E+04  0.0000E+00
363265.E-02  1.6386E+04  0.0000E+00
370833.E-02  1.6386E+04  0.0000E+00
378401.E-02  1.9954E+04  0.0000E+00
385969.E-02  2.1012E+04  0.0000E+00
393537.E-02  2.7751E+04  0.0000E+00
401105.E-02  2.8148E+04  0.0000E+00
408673.E-02  3.4755E+04  0.0000E+00
416241.E-02  3.5812E+04  0.0000E+00
423809.E-02  3.9777E+04  0.0000E+00
431377.E-02  4.2288E+04  0.0000E+00
438945.E-02  4.3741E+04  0.0000E+00
446513.E-02  5.5635E+04  0.0000E+00
454081.E-02  5.5635E+04  0.0000E+00
461649.E-02  5.8542E+04  0.0000E+00
469217.E-02  6.5282E+04  0.0000E+00
476785.E-02  6.5150E+04  0.0000E+00
484353.E-02  6.6075E+04  0.0000E+00
491921.E-02  6.6075E+04  0.0000E+00
499489.E-02  6.7925E+04  0.0000E+00
507057.E-02  5.5238E+04  0.0000E+00
514625.E-02  5.2860E+04  0.0000E+00
522193.E-02  5.1142E+04  0.0000E+00
529761.E-02  4.3741E+04  0.0000E+00
537329.E-02  3.2773E+04  0.0000E+00
544897.E-02  3.2112E+04  0.0000E+00
552465.E-02  2.6958E+04  0.0000E+00
560033.E-02  2.0087E+04  0.0000E+00
567601.E-02  1.6122E+04  0.0000E+00
575169.E-02  1.2686E+04  0.0000E+00
582737.E-02  9.2504E+03  0.0000E+00
590305.E-02  5.1538E+03  0.0000E+00
597873.E-02  4.0966E+03  0.0000E+00
605441.E-02  3.3037E+03  0.0000E+00
613009.E-02  2.9073E+03  0.0000E+00
620577.E-02  1.0572E+03  0.0000E+00
628145.E-02  1.0572E+03  0.0000E+00
635713.E-02  1.3215E+02  0.0000E+00
643281.E-02  6.6074E+02  0.0000E+00
650849.E-02  2.6430E+02  0.0000E+00
658417.E-02  0.0000E+00  0.0000E+00
665985.E-02  0.0000E+00  0.0000E+00
673553.E-02  0.0000E+00  0.0000E+00
681121.E-02  0.0000E+00  0.0000E+00
688689.E-02  0.0000E+00  0.0000E+00
696257.E-02  0.0000E+00  0.0000E+00
703825.E-02  0.0000E+00  0.0000E+00
711393.E-02  0.0000E+00  0.0000E+00
718961.E-02  0.0000E+00  0.0000E+00
726529.E-02  0.0000E+00  0.0000E+00
734097.E-02  0.0000E+00  0.0000E+00
741665.E-02  0.0000E+00  0.0000E+00
749233.E-02  0.0000E+00  0.0000E+00
756801.E-02  0.0000E+00  0.0000E+00
