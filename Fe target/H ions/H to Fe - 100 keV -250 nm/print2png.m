function print2png(h,sz,fname)

set(h, 'PaperUnits','centimeters')
set(h,'PaperSize',sz)
set(h,'PaperPosition',[ 1 1 sz(1) sz(2)])

print(h,'-dpng','-r600','-svgconvert',fname)
