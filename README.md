# Irradopt

Optimization of ion irradiation conditions for metallic foils using SRIM.

# Description

This repo contains the data+code for the BSc thesis of of @KoutsomK.  The SRIM code (http://srim.org/) was employed to simulate the irradiation of thin Fe and W foils by various ions. The aim was to maximize the radiation damage in the foils while at the same time keeping ion implantations at a minimum.

The folders contain the SRIM input and output files, the OCTAVE scripts (https://octave.org) used for data processing and the resulting plots. Some processed data are summarized in excel worksheets.

For example the following graphs show the damage cross section for irradiation of a 1um thick Fe foil with various ions. 

<p>
<img src="Fe%20target/H%20ions/H%20to%20Fe%20-%20300%20keV%20-%201%20um/Fig2.png" width="45%">
<img src="Fe%20target/H%20ions/H%20to%20Fe%20-%20300%20keV%20-%201%20um/Fig2.png" width="45%">
</p>
<p>
<img src="Fe%20target/O%20ions/O%20to%20Fe%20-%202.5%20MeV%20-%201%20um/Fig2.png" width="45%">
<img src="Fe%20target/Fe%20ions/Fe%20to%20Fe%20-%205.5%20MeV%20-%201%20um/Fig2.png" width="45%">
</p>

## Usage
In order to reproduce these calculations and the associated data processing you will need to install 

- [SRIM](http://srim.org/) for running the simulations
- [GNU OCTAVE](https://octave.org) for data processing & plotting
- [srim2mat](https://gitlab.com/ir2-lab/srim2mat) to import SRIM output data to OCTAVE




