====== H (170) into Layer 1                     =======
        SRIM-2013.00
=======================================
      Data  of  TRIM  Calculation      
=======================================
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
Ion =  H ( 1)   Ion Mass= 001.0080
Energy  = 1700000.E-04 keV
Ion Angle to Surface = 0 degrees
============= TARGET MATERIAL ======================================
  Layer # 1 - Layer 1
  Layer # 1 - Bottom Depth=     1.E+04 A
  Layer # 1- Density = 6.338E22 atoms/cm3 = 19.35 g/cm3
  Layer # 1-  W = 100  Atomic Percent = 100  Mass Percent
====================================================================
Target energies for target atom =  W
    Displacement = 90 eV, Binding = 3 eV, Surface = 8.68 eV
====================================================================
Depth Range of Tabulated Data=  000000.E+00  -  116000.E-01  Angstroms
====================================================================
Total Ions calculated =  009999
Average Range         =   62053.E-01 Angstroms
Average Straggling    =   13630.E-01 Angstroms
Average Vacancy/Ion   =   16685.E-04
====================================================================
Total Backscattered Ions=  65 
Total Transmitted Ions  =  0 
====================================================================
See files SRIM Outputs\*.txt for tables :
   TDATA.txt    -- Details of the calculation.
   RANGE.txt    -- Final distribution of Ions/Recoils
   VACANCY.txt  -- Distribution of Vacancies
   IONIZ.txt    -- Distribution of Ionization
   PHONON.txt   -- Distribution of Phonons
   E2RECOIL.txt -- Energy transferred to recoils
   NOVAC.txt    -- Replacement collisions
   LATERAL.txt  -- Lateral Spread of Ions
