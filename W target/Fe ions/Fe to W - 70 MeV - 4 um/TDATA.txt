====== Fe (10) into Layer 1                     =======
        SRIM-2013.00
=======================================
      Data  of  TRIM  Calculation      
=======================================
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
Ion = Fe (26)   Ion Mass= 055.9350
Energy  = 7000000.E-02 keV
Ion Angle to Surface = 0 degrees
============= TARGET MATERIAL ======================================
  Layer # 1 - Layer 1
  Layer # 1 - Bottom Depth=     7.E+04 A
  Layer # 1- Density = 6.338E22 atoms/cm3 = 19.35 g/cm3
  Layer # 1-  W = 100  Atomic Percent = 100  Mass Percent
====================================================================
Target energies for target atom =  W
    Displacement = 90 eV, Binding = 3 eV, Surface = 8.68 eV
====================================================================
Depth Range of Tabulated Data=  000000.E+00  -  653000.E-01  Angstroms
====================================================================
Total Ions calculated =  009999
Average Range         =   49526.E+00 Angstroms
Average Straggling    =   44204.E-01 Angstroms
Average Vacancy/Ion   =   70684.E-01
====================================================================
Total Backscattered Ions=  0 
Total Transmitted Ions  =  0 
====================================================================
See files SRIM Outputs\*.txt for tables :
   TDATA.txt    -- Details of the calculation.
   RANGE.txt    -- Final distribution of Ions/Recoils
   VACANCY.txt  -- Distribution of Vacancies
   IONIZ.txt    -- Distribution of Ionization
   PHONON.txt   -- Distribution of Phonons
   E2RECOIL.txt -- Energy transferred to recoils
   NOVAC.txt    -- Replacement collisions
   LATERAL.txt  -- Lateral Spread of Ions
