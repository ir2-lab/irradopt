function fig_2(x, y, z, u,dep, width, ymean) %% x -> x-axis data, y-> y-axis data, z-> normalisation exponent, u -> upper y-limit
                                             %% dep , width as in main script, ymean -> mean value of y
clrs = [0   0.4470   0.7410;
   0.8500   0.3250   0.0980;
   0.9290   0.6940   0.1250;
   0.4940   0.1840   0.5560;
   0.4660   0.6740   0.1880;
   0.3010   0.7450   0.9330;
   0.6350   0.0780   0.1840];
q = -log10(z);
exp=floor(log10(ymean));
pow=10^exp;

plot(width/10000,z*ymean*ones(size(width)), '--','color',clrs(2,:),'linewidth', 1.5)
hold on
plot (x/10000, y*z, 'color', clrs(1,:), 'LineWidth', 1.5)
ylim([0 u(2)])
xlim([0 u(1)])
plot([dep dep]/10000, ylim, '--', 'color', 'k' )
hold off

str = [' Mean \sigma_d = '; num2str(ymean/pow,'%.2d'), ' 10^{', num2str(exp),'}'];
t=text(1.2*dep/10000 , ymean*z, str,'edgecolor', clrs(2,:),...
  'VerticalAlignment','middle',...
  'HorizontalAlignment','left',...
  'fontsize', 8);


ylabel ({['\sigma_d (10^{', num2str(q), '}cm^2)']})
xlabel('Depth (\mum)')
title('Damage cross-section')
h=text(dep/10/10000,0.9*u(2),'4.5 MeV Fe on W','edgecolor', 'k');
print2png(gcf,[12 9],'Fig2')
