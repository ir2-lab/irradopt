function fig_5(x, y, u,dep, width, ymean) %% x -> x-axis data , y-> y-axis data
                                          %% u upper limit vector
clrs = [0   0.4470   0.7410;
   0.8500   0.3250   0.0980;
   0.9290   0.6940   0.1250;
   0.4940   0.1840   0.5560;
   0.4660   0.6740   0.1880;
   0.3010   0.7450   0.9330;
   0.6350   0.0780   0.1840];

plot(width/10000,ymean/1000*ones(size(width)), '--','color',clrs(2,:),'linewidth', 1.5)
hold on
plot(x/10000, y/1000, 'color', clrs(1,:), 'LineWidth', 1.5)
upl=ylim;
plot([dep dep]/10000, [0,u(2)], '--', 'color', 'k')
hold off

ylim([0 u(2)])
xlim([0 u(1)])

str = [' Mean \sigma_T/\sigma_{PKA} =' num2str(ymean/1000,'%.2f'), ' keV'];
t=text(1.1*dep/10000 ,0.8*ymean/1000, str,'edgecolor', clrs(2,:),...
  'VerticalAlignment','top',...
  'HorizontalAlignment','left',...
  'fontsize', 8);

ylabel ({['\sigma_T/\sigma_{PKA} (keV)']})
xlabel('Depth (\mum)')
title('Recoil energy per PKA')
h=text(dep/10/10000, 0.9*u(2),'4.5 MeV Fe on W','edgecolor', 'k');
print2png(gcf,[12 9],'Fig5')
