====== Fe (10) into Layer 1                     =======
        SRIM-2013.00
==================================================
      ION and final RECOIL ATOM Distributions     
 See  SRIM Outputs\TDATA.txt for calculation details
==================================================
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
----------------------------------------------------------------
  There are NO RECOILS unless a full TRIM calculation is made.   
----------------------------------------------------------------
See file :  SRIM Outputs\TDATA.txt   for details of calculation
 Ion    = Fe   Energy = 4500  keV
============= TARGET MATERIAL ======================================
Layer  1 : Layer 1
Layer Width =     2.E+04 A   Layer # 1- Density = 6.338E22 atoms/cm3 = 19.35 g/cm3
  Layer # 1-  W = 100  Atomic Percent = 100  Mass Percent
====================================================================
 Total Ions calculated =9999.00
 Ion Average Range =   932.6E+01 A   Straggling =   274.1E+01 A
 Ion Lateral Range =   280.5E+01 A   Straggling =   345.3E+01 A
 Ion Radial  Range =   438.2E+01 A   Straggling =   211.8E+01 A
====================================================================
 Transmitted Ions =;  Backscattered Ions =71
 (These are not included in Skewne- and Kurtosis below.)

 Range Skewne- = -000.5451  &= [-(X-Rp)^3]/[N*Straggle^3]
 Range Kurtosis = 003.0867  &= [-(X-Rp)^4]/[N*Straggle^4]
 Statistical definitions above are those used in VLSI implant modelling.
====================================================================
=================================================================
  Table Distribution Units are >>>  (Atoms/cm3) / (Atoms/cm2)  <<<  
=================================================================
   DEPTH         Fe       Recoil     
   (Ang.)       Ions     Distribution
-----------  ----------  ------------
195010.E-03  5.1287E+01  0.0000E+00
390010.E-03  2.5644E+02  0.0000E+00
585010.E-03  3.5901E+02  0.0000E+00
780010.E-03  4.6158E+02  0.0000E+00
975010.E-03  5.1287E+02  0.0000E+00
117001.E-02  6.6673E+02  0.0000E+00
136501.E-02  5.1287E+02  0.0000E+00
156001.E-02  8.7188E+02  0.0000E+00
175501.E-02  6.1545E+02  0.0000E+00
195001.E-02  8.2059E+02  0.0000E+00
214501.E-02  9.2317E+02  0.0000E+00
234001.E-02  1.1283E+03  0.0000E+00
253501.E-02  1.2309E+03  0.0000E+00
273001.E-02  1.0770E+03  0.0000E+00
292501.E-02  1.4873E+03  0.0000E+00
312001.E-02  1.9489E+03  0.0000E+00
331501.E-02  2.1028E+03  0.0000E+00
351001.E-02  2.3079E+03  0.0000E+00
370501.E-02  2.4618E+03  0.0000E+00
390001.E-02  2.2053E+03  0.0000E+00
409501.E-02  2.4618E+03  0.0000E+00
429001.E-02  3.1798E+03  0.0000E+00
448501.E-02  2.5131E+03  0.0000E+00
468001.E-02  3.1798E+03  0.0000E+00
487501.E-02  3.4362E+03  0.0000E+00
507001.E-02  2.9747E+03  0.0000E+00
526501.E-02  3.7440E+03  0.0000E+00
546001.E-02  4.7697E+03  0.0000E+00
565501.E-02  5.2313E+03  0.0000E+00
585001.E-02  4.9749E+03  0.0000E+00
604501.E-02  5.8980E+03  0.0000E+00
624001.E-02  6.4109E+03  0.0000E+00
643501.E-02  5.0261E+03  0.0000E+00
663001.E-02  7.4366E+03  0.0000E+00
682501.E-02  7.8982E+03  0.0000E+00
702001.E-02  6.8725E+03  0.0000E+00
721501.E-02  8.2572E+03  0.0000E+00
741001.E-02  8.0008E+03  0.0000E+00
760501.E-02  9.8471E+03  0.0000E+00
780001.E-02  9.6420E+03  0.0000E+00
799501.E-02  1.0411E+04  0.0000E+00
819001.E-02  1.2104E+04  0.0000E+00
838501.E-02  1.1334E+04  0.0000E+00
858001.E-02  1.2463E+04  0.0000E+00
877501.E-02  1.1796E+04  0.0000E+00
897001.E-02  1.3796E+04  0.0000E+00
916501.E-02  1.4822E+04  0.0000E+00
936001.E-02  1.4412E+04  0.0000E+00
955501.E-02  1.6002E+04  0.0000E+00
975001.E-02  1.3848E+04  0.0000E+00
994501.E-02  1.4207E+04  0.0000E+00
101400.E-01  1.4155E+04  0.0000E+00
103350.E-01  1.4104E+04  0.0000E+00
105300.E-01  1.4822E+04  0.0000E+00
107250.E-01  1.5181E+04  0.0000E+00
109200.E-01  1.4360E+04  0.0000E+00
111150.E-01  1.5232E+04  0.0000E+00
113100.E-01  1.4514E+04  0.0000E+00
115050.E-01  1.3540E+04  0.0000E+00
117000.E-01  1.3181E+04  0.0000E+00
118950.E-01  1.2617E+04  0.0000E+00
120900.E-01  1.2822E+04  0.0000E+00
122850.E-01  1.0463E+04  0.0000E+00
124800.E-01  9.1291E+03  0.0000E+00
126750.E-01  9.5394E+03  0.0000E+00
128700.E-01  7.3854E+03  0.0000E+00
130650.E-01  6.8725E+03  0.0000E+00
132600.E-01  5.8980E+03  0.0000E+00
134550.E-01  5.2313E+03  0.0000E+00
136500.E-01  4.6158E+03  0.0000E+00
138450.E-01  3.8465E+03  0.0000E+00
140400.E-01  3.5388E+03  0.0000E+00
142350.E-01  2.3592E+03  0.0000E+00
144300.E-01  1.6925E+03  0.0000E+00
146250.E-01  1.3335E+03  0.0000E+00
148200.E-01  7.1802E+02  0.0000E+00
150150.E-01  8.2059E+02  0.0000E+00
152100.E-01  8.2059E+02  0.0000E+00
154050.E-01  5.6416E+02  0.0000E+00
156000.E-01  2.5644E+02  0.0000E+00
157950.E-01  2.5644E+02  0.0000E+00
159900.E-01  2.0515E+02  0.0000E+00
161850.E-01  0.0000E+00  0.0000E+00
163800.E-01  1.0257E+02  0.0000E+00
165750.E-01  5.1287E+01  0.0000E+00
167700.E-01  0.0000E+00  0.0000E+00
169650.E-01  0.0000E+00  0.0000E+00
171600.E-01  0.0000E+00  0.0000E+00
173550.E-01  0.0000E+00  0.0000E+00
175500.E-01  0.0000E+00  0.0000E+00
177450.E-01  0.0000E+00  0.0000E+00
179400.E-01  0.0000E+00  0.0000E+00
181350.E-01  0.0000E+00  0.0000E+00
183300.E-01  0.0000E+00  0.0000E+00
185250.E-01  0.0000E+00  0.0000E+00
187200.E-01  0.0000E+00  0.0000E+00
189150.E-01  0.0000E+00  0.0000E+00
191100.E-01  0.0000E+00  0.0000E+00
193050.E-01  0.0000E+00  0.0000E+00
195000.E-01  0.0000E+00  0.0000E+00
