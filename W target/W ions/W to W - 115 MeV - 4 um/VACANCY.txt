====== W (10) into Layer 1                      =======
        SRIM-2013.00
==========================================
    Ion and Target VACANCY production     
  See SRIM Outputs\TDATA.txt for calc. details
==========================================
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
 See file :  SRIM Outputs\TDATA.txt for calculation data
 Ion    =  W   Energy = 115000 keV
============= TARGET MATERIAL ======================================
Layer  1 : Layer 1
Layer Width =     7.E+04 A ;
  Layer # 1- Density = 6.338E22 atoms/cm3 = 19.35 g/cm3
  Layer # 1-  W = 100  Atomic Percent = 100  Mass Percent
====================================================================
 Total Ions calculated =9999.00
 Total Target Vacancies     = 43080 /Ion

==========================================================
  Table Units are  >>>> Vacancies/(Angstrom-Ion)  <<<<  
==========================================================
   TARGET     VACANCIES     VACANCIES 
   DEPTH         by            by     
   (Ang.)       IONS         RECOILS  
-----------  -----------  ------------
700010.E-03  1017.46E-05  2043.67E-04  
140001.E-02  1055.32E-05  2380.32E-04  
210001.E-02  1064.51E-05  2092.99E-04  
280001.E-02  1085.11E-05  2156.20E-04  
350001.E-02  1094.27E-05  2461.25E-04  
420001.E-02  1118.57E-05  2459.07E-04  
490001.E-02  1127.98E-05  2170.16E-04  
560001.E-02  1151.09E-05  2332.11E-04  
630001.E-02  1155.17E-05  2537.40E-04  
700001.E-02  1179.29E-05  2659.10E-04  
770001.E-02  1193.42E-05  2536.10E-04  
840001.E-02  1209.32E-05  2489.96E-04  
910001.E-02  1225.74E-05  2553.94E-04  
980001.E-02  1255.83E-05  2671.56E-04  
105000.E-01  1272.91E-05  2820.84E-04  
112000.E-01  1294.29E-05  2848.66E-04  
119000.E-01  1306.36E-05  2521.96E-04  
126000.E-01  1332.03E-05  3032.35E-04  
133000.E-01  1344.55E-05  2862.47E-04  
140000.E-01  1366.97E-05  3352.17E-04  
147000.E-01  1395.77E-05  3048.27E-04  
154000.E-01  1425.14E-05  3175.54E-04  
161000.E-01  1443.90E-05  3371.81E-04  
168000.E-01  1462.19E-05  3386.79E-04  
175000.E-01  1491.62E-05  3516.17E-04  
182000.E-01  1509.08E-05  3676.81E-04  
189000.E-01  1550.77E-05  3689.93E-04  
196000.E-01  1564.64E-05  3761.08E-04  
203000.E-01  1602.76E-05  4170.79E-04  
210000.E-01  1639.14E-05  4083.47E-04  
217000.E-01  1671.65E-05  4205.79E-04  
224000.E-01  1687.48E-05  4385.19E-04  
231000.E-01  1723.30E-05  4460.69E-04  
238000.E-01  1757.48E-05  4497.03E-04  
245000.E-01  1792.48E-05  4408.30E-04  
252000.E-01  1823.67E-05  4901.14E-04  
259000.E-01  1859.46E-05  4814.92E-04  
266000.E-01  1892.86E-05  5339.26E-04  
273000.E-01  1945.45E-05  5244.36E-04  
280000.E-01  1973.01E-05  5002.85E-04  
287000.E-01  2014.64E-05  5385.83E-04  
294000.E-01  2057.21E-05  5591.61E-04  
301000.E-01  2111.67E-05  5782.70E-04  
308000.E-01  2154.77E-05  6039.04E-04  
315000.E-01  2198.81E-05  6065.60E-04  
322000.E-01  2237.30E-05  6373.10E-04  
329000.E-01  2295.52E-05  6549.92E-04  
336000.E-01  2332.48E-05  6841.76E-04  
343000.E-01  2398.93E-05  6879.78E-04  
350000.E-01  2437.93E-05  7338.79E-04  
357000.E-01  2505.84E-05  7657.20E-04  
364000.E-01  2560.57E-05  7777.36E-04  
371000.E-01  2617.96E-05  7717.45E-04  
378000.E-01  2649.19E-05  8391.95E-04  
385000.E-01  2695.48E-05  8747.80E-04  
392000.E-01  2738.00E-05  9012.36E-04  
399000.E-01  2764.61E-05  9495.86E-04  
406000.E-01  2818.68E-05  1002.64E-03  
413000.E-01  2854.17E-05  1024.34E-03  
420000.E-01  2827.77E-05  1072.68E-03  
427000.E-01  2758.65E-05  1113.43E-03  
434000.E-01  2692.41E-05  1093.23E-03  
441000.E-01  2650.97E-05  1201.09E-03  
448000.E-01  2620.70E-05  1209.62E-03  
455000.E-01  2721.59E-05  1335.35E-03  
462000.E-01  2789.55E-05  1311.98E-03  
469000.E-01  2844.56E-05  1357.71E-03  
476000.E-01  2903.39E-05  1399.27E-03  
483000.E-01  3002.24E-05  1420.32E-03  
490000.E-01  3013.22E-05  1478.21E-03  
497000.E-01  3024.17E-05  1522.51E-03  
504000.E-01  3012.67E-05  1479.20E-03  
511000.E-01  3060.32E-05  1531.30E-03  
518000.E-01  3083.18E-05  1534.68E-03  
525000.E-01  3037.45E-05  1532.65E-03  
532000.E-01  2979.33E-05  1443.16E-03  
539000.E-01  2874.56E-05  1405.63E-03  
546000.E-01  2670.04E-05  1267.89E-03  
553000.E-01  2478.12E-05  1204.11E-03  
560000.E-01  2273.44E-05  1126.01E-03  
567000.E-01  2090.32E-05  9960.90E-04  
574000.E-01  1780.95E-05  8315.02E-04  
581000.E-01  1510.91E-05  6865.35E-04  
588000.E-01  1205.76E-05  5410.56E-04  
595000.E-01  9577.67E-06  4191.64E-04  
602000.E-01  6501.79E-06  2860.95E-04  
609000.E-01  4383.72E-06  1941.67E-04  
616000.E-01  2710.27E-06  1115.85E-04  
623000.E-01  1582.16E-06  6014.40E-05  
630000.E-01  8369.41E-07  3064.22E-05  
637000.E-01  2956.01E-07  1220.05E-05  
644000.E-01  1587.30E-07  7014.38E-06  
651000.E-01  5186.23E-08  1687.82E-06  
658000.E-01  8429.41E-09  1118.88E-07  
665000.E-01  0000.00E+00  0000.00E+00  
672000.E-01  0000.00E+00  0000.00E+00  
679000.E-01  0000.00E+00  0000.00E+00  
686000.E-01  0000.00E+00  0000.00E+00  
693000.E-01  0000.00E+00  0000.00E+00  
700000.E-01  0000.00E+00  0000.00E+00  

 To convert to Energy Lost - multiply by Average Binding Energy =  3  eV/Vacancy
