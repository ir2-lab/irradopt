clear all

N = 6.338E22;  %%atomic density se cm-3
data = srim2mat('.');
%function out = srim2mat('W to Fe_ 8MeV_1micron')
% The structure out has the following fields:
%   x   :  target depth (Angstroem)
%   Vi  :  vacancies from ions (Atoms/ion-Angstroem)
%   Vr  :  vacancies from recoils (Atoms/ion-Angstroem)
%   RC  :  replacement collisions (Atoms/ion-Angstroem)
%   EIi :  ionization energy by ions (eV/ion-Angstroem)
%   EIr :  ionization energy by target recoils (eV/ion-Angstroem)
%   EPi :  phonon energy by ions (eV/ion-Angstroem)
%   EPr :  phonon energy by target recoils (eV/ion-Angstroem)
%   ERi :  recoil energy from ions (eV/ion-Angstroem)
%   ERr :  recoil energy absorbed by target recoils (eV/ion-Angstroem)
%   Ri  :  range distribution of ions (atoms/cm3) / (atoms/cm2)
%   Rr  :  range distribution of recoils (atoms/cm3) / (atoms/cm2)
%   parsedFiles : cell array with all succesfully parsed files
%   failedFiles : cell array with all failed files

n_ions = 9999;                                            % number of ions

dep = 10000;                                              %real depth in angstrom
erec = 1e8*data.ERi/N;                                    % eV cm energy to recoil per atom per unit flux
sigma_pka= 1e8*(data.Vi)/N;                               % pka displacements per atom per unit flux
sigmad = 1e8*(data.Vi+data.Vr)/N;                         % cm2 , total vacancies per atom per unit flux / damage cross section �(xi)
i = find(data.x <= dep);

sigma_mean = mean(sigmad(i))                              %se cm2  mean damage cross section
dsigma = std(sigmad(i))/sqrt(n_ions)                      %standard deviation of damage cross section

erec_mean = mean(erec(i))                                 %mean energy to recoil per atom per unit flux
erec_pka = mean(erec(i)./sigma_pka(i))                    %recoil energy per PKA
derec_pka = std(erec(i)./sigma_pka(i))/sqrt(n_ions)       %standard deviatonn of mean recoil energy per PKA

disp_pka = mean(sigmad(i)./sigma_pka(i))                  %mean displacemtns per PKA
d_disp_pka = std(sigmad(i)./sigma_pka(i))/sqrt(n_ions)    %standard deviation of displacemtns per PKA

imp_mean = mean (data.Ri(i)/N./sigmad(i))                 %mean number of implantations per pka
d_imp = std(data.Ri(i)/N./sigmad(i))/sqrt(n_ions)         %standard deviation of displacements per PKA
width = data.x(i);                                        %real depth vector
ion_mean = mean(data.Ri(i)/N)                             %mean implantation cross section

figure 1
clf
##plot (data.x, data.Ri/N)
##hold on
##y=ylim;
##plot([dep dep], y, 'color', 'r', 'LineWidth', 1 )
##hold off
##ylabel ('Inplanted ions per atom per unit fluence (cm^2)')
##xlabel('Depth (angstrom)')
##title('Ion of He distribution in W ')
##h=text(dep/10,y(2)*0.9,'1.1 MeV He on W','edgecolor', 'k');
##print -dpng Fig1
fig_1(data.x, data.Ri/N,1e19,[2.3 6],dep,width,ion_mean)


figure 2
clf
##plot(width,sigma_mean*ones(size(width)), '--m')
##hold on
##plot (data.x, sigmad)
##y=ylim ;
##plot([dep dep], [0 y(2)], 'color', 'r', 'LineWidth', 1 )
##hold off
##lgd2 = sprintf('Mean damage cross section = %.2d cm^2', sigma_mean );
##legend(lgd2)
##ylabel ('\sigma_d (cm^2)')
##xlabel('Depth (angstrom)')
##title('Vacancies per Target Atom per unit fluence in W ')
##h=text(dep/10,y(2)*0.9,'1.1 MeV He on W','edgecolor', 'k');
##print -dpng Fig2
fig_2(data.x, sigmad,1e18, [2.3 10],dep,width,sigma_mean)

figure 3
clf
##%plot(width,erec_mean*ones(size(width)), '--m')
##hold on
##plot (data.x, erec)
##y=ylim;
##plot([dep dep], [0 y(2)], 'color', 'r', 'LineWidth', 1 )
##hold off
##ylabel ('Recoil energy per atom (eV*cm^2)')
##xlabel('Depth (angstrom)')
##title('Recoil energy per Target Atom per unit fluence in W ')
##h=text(dep/10,y(2)*0.9,'1.1 MeV He on W','edgecolor', 'k');
##print -dpng Fig3
fig_3(data.x, erec, 1e15,[2.5 3], dep)

figure 4
clf
##semilogy(width,imp_mean*ones(size(width)), '--m')
##hold on
##semilogy (data.x, data.Ri/N./sigmad)
##y=ylim;
##plot([dep dep], [y(1) y(2)], 'color', 'r', 'LineWidth', 1 )
##hold off
##lgd4 = sprintf('Mean implantations per dpa = %.2d', imp_mean);
##legend (lgd4)
##xlabel('Depth (angstrom)')
##title('Implanted ions per dpa')
##h=text(dep/10,y(2)*0.7,'1.1 MeV He on W','edgecolor', 'k');
##print -dpng Fig4
fig_4(data.x, data.Ri/N./sigmad,[2 1e-4 1e1], dep, width, imp_mean)

figure 5
clf
##plot(width,mean(erec(i)./sigma_pka(i))*ones(size(width)), '--m')
##hold on
##plot(data.x, erec./sigma_pka)
##y=ylim;
##plot([dep dep], [0 y(2)], 'color', 'r', 'LineWidth', 1 )
##hold off
##lgd5 = sprintf('Mean PKA recoil energy = %.2d eV', mean(erec(i)./sigma_pka(i)) );
##legend (lgd5)
##ylabel ('eV')
##xlabel('Depth (angstrom)')
##title ('Recoil energy per pka')
##h=text(dep/10,y(2)*0.8,'1.1 MeV He on W','edgecolor', 'k');
##print -dpng Fig5
fig_5(data.x, erec./sigma_pka,[2 3],dep, width, mean(erec(i)./sigma_pka(i)))

figure 6
clf
##plot(width,mean(sigmad(i)./sigma_pka(i))*ones(size(width)), '--m')
##hold on
##plot(data.x, sigmad./sigma_pka)
##y=ylim;
##plot([dep dep], [0 y(2)], 'color', 'r', 'LineWidth', 1 )
##hold off
##lgd6 = sprintf('Mean displacements per PKA = %.2d', mean(sigmad(i)./sigma_pka(i)) );
##legend (lgd6)
##xlabel('Depth (angstrom)')
##title('Displaments per pka')
##h=text(2.5*dep,y(2)*0.8,'1.1 MeV He on W','edgecolor', 'k');
##print -dpng Fig6
fig_6(data.x, sigmad./sigma_pka,[2 5], dep, width ,mean(sigmad(i)./sigma_pka(i)))

