function fig_6(x, y, u, dep, width, ymean) %% x -> x-axis data , y-> y-axis data
                                           %% u upper limit vector
clrs = [0   0.4470   0.7410;
   0.8500   0.3250   0.0980;
   0.9290   0.6940   0.1250;
   0.4940   0.1840   0.5560;
   0.4660   0.6740   0.1880;
   0.3010   0.7450   0.9330;
   0.6350   0.0780   0.1840];


plot(width/10000,ymean*ones(size(width)), '--','color',clrs(2,:),'linewidth', 1.5)
hold on

ylim([0 u(2)])
xlim([0 u(1)])

plot(x/10000, y, 'color', clrs(1,:), 'LineWidth', 1.5)
plot([dep dep]/10000, ylim, '--', 'color', 'k')
hold off


str = [' Mean \sigma_d /\sigma_{PKA} = ' num2str(ymean,'%.2d')];
t=text(1.3*dep/10000 , ymean, str,'edgecolor', clrs(2,:),...
  'VerticalAlignment','middle',...
  'HorizontalAlignment','left',...
  'fontsize', 8);


ylabel ({['\sigma_d/\sigma_{PKA}']})
xlabel('Depth (\mum)')
title('Displacements per PKA')
h=text(dep/10/10000, 0.9*u(2),'390 keV He on W','edgecolor', 'k');
print2png(gcf,[12 9],'Fig6')
