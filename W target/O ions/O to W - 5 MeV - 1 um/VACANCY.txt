====== O (10) into Layer 1                      =======
        SRIM-2013.00
==========================================
    Ion and Target VACANCY production     
  See SRIM Outputs\TDATA.txt for calc. details
==========================================
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
 See file :  SRIM Outputs\TDATA.txt for calculation data
 Ion    =  O   Energy = 5000 keV
============= TARGET MATERIAL ======================================
Layer  1 : Layer 1
Layer Width =     2.E+04 A ;
  Layer # 1- Density = 6.338E22 atoms/cm3 = 19.35 g/cm3
  Layer # 1-  W = 100  Atomic Percent = 100  Mass Percent
====================================================================
 Total Ions calculated =9999.00
 Total Target Vacancies     = 549 /Ion

==========================================================
  Table Units are  >>>> Vacancies/(Angstrom-Ion)  <<<<  
==========================================================
   TARGET     VACANCIES     VACANCIES 
   DEPTH         by            by     
   (Ang.)       IONS         RECOILS  
-----------  -----------  ------------
243010.E-03  9124.27E-07  7054.65E-06  
486010.E-03  1200.10E-06  5002.24E-06  
729010.E-03  1233.03E-06  5141.50E-06  
972010.E-03  1292.29E-06  7088.48E-06  
121501.E-02  1324.80E-06  5992.42E-06  
145801.E-02  1282.00E-06  6485.83E-06  
170101.E-02  1347.03E-06  6276.97E-06  
194401.E-02  1382.42E-06  5718.74E-06  
218701.E-02  1365.96E-06  6792.80E-06  
243001.E-02  1411.23E-06  9266.20E-06  
267301.E-02  1471.31E-06  7891.12E-06  
291601.E-02  1492.72E-06  7690.88E-06  
315901.E-02  1504.24E-06  7236.42E-06  
340201.E-02  1498.89E-06  8542.02E-06  
364501.E-02  1583.26E-06  8160.08E-06  
388801.E-02  1618.24E-06  7930.16E-06  
413101.E-02  1650.35E-06  8312.45E-06  
437401.E-02  1705.51E-06  7757.96E-06  
461701.E-02  1749.15E-06  8964.08E-06  
486001.E-02  1823.65E-06  8713.49E-06  
510301.E-02  1849.59E-06  7380.66E-06  
534601.E-02  1794.84E-06  9886.66E-06  
558901.E-02  2003.13E-06  9734.90E-06  
583201.E-02  1968.55E-06  9704.93E-06  
607501.E-02  2055.41E-06  9655.54E-06  
631801.E-02  2166.56E-06  1378.79E-05  
656101.E-02  2196.61E-06  8311.65E-06  
680401.E-02  2294.58E-06  1133.12E-05  
704701.E-02  2364.15E-06  1229.34E-05  
729001.E-02  2486.41E-06  1238.00E-05  
753301.E-02  2613.19E-06  1286.90E-05  
777601.E-02  2756.03E-06  1233.91E-05  
801901.E-02  2781.56E-06  1405.40E-05  
826201.E-02  2852.77E-06  1468.07E-05  
850501.E-02  2980.38E-06  1372.20E-05  
874801.E-02  3187.44E-06  1654.14E-05  
899101.E-02  3370.62E-06  1918.04E-05  
923401.E-02  3416.73E-06  1820.13E-05  
947701.E-02  3696.24E-06  1757.12E-05  
972001.E-02  3754.69E-06  1859.26E-05  
996301.E-02  4065.90E-06  1903.49E-05  
102060.E-01  4170.45E-06  1866.95E-05  
104490.E-01  4439.67E-06  2265.74E-05  
106920.E-01  4746.35E-06  2295.35E-05  
109350.E-01  5074.84E-06  2447.37E-05  
111780.E-01  5219.33E-06  2569.95E-05  
114210.E-01  5644.15E-06  2738.09E-05  
116640.E-01  5854.91E-06  2641.42E-05  
119070.E-01  6196.58E-06  3052.81E-05  
121500.E-01  6925.53E-06  3100.95E-05  
123930.E-01  7367.64E-06  3393.21E-05  
126360.E-01  7988.65E-06  3731.17E-05  
128790.E-01  8460.79E-06  3844.76E-05  
131220.E-01  8946.08E-06  4076.17E-05  
133650.E-01  9743.11E-06  4262.60E-05  
136080.E-01  1033.41E-05  4568.32E-05  
138510.E-01  1100.53E-05  5082.58E-05  
140940.E-01  1135.28E-05  4854.52E-05  
143370.E-01  1214.53E-05  5519.12E-05  
145800.E-01  1291.46E-05  5306.35E-05  
148230.E-01  1365.95E-05  5453.58E-05  
150660.E-01  1426.73E-05  5761.22E-05  
153090.E-01  1553.59E-05  6019.06E-05  
155520.E-01  1541.44E-05  5786.58E-05  
157950.E-01  1554.17E-05  5975.63E-05  
160380.E-01  1571.80E-05  5798.07E-05  
162810.E-01  1562.99E-05  5509.78E-05  
165240.E-01  1543.29E-05  5379.61E-05  
167670.E-01  1430.44E-05  4794.78E-05  
170100.E-01  1357.59E-05  4186.34E-05  
172530.E-01  1267.13E-05  3745.07E-05  
174960.E-01  1122.90E-05  3286.75E-05  
177390.E-01  9592.59E-06  2702.45E-05  
179820.E-01  7969.32E-06  2087.89E-05  
182250.E-01  6477.66E-06  1562.50E-05  
184680.E-01  4863.26E-06  1219.42E-05  
187110.E-01  3710.64E-06  8555.30E-06  
189540.E-01  2105.63E-06  4701.76E-06  
191970.E-01  1325.21E-06  2687.30E-06  
194400.E-01  7399.87E-07  1437.66E-06  
196830.E-01  3621.81E-07  5984.33E-07  
199260.E-01  1868.50E-07  3699.62E-07  
201690.E-01  7366.99E-08  1458.08E-07  
204120.E-01  4568.36E-08  9340.61E-08  
206550.E-01  1028.91E-08  1358.73E-08  
208980.E-01  0000.00E+00  0000.00E+00  
211410.E-01  0000.00E+00  0000.00E+00  
213840.E-01  0000.00E+00  0000.00E+00  
216270.E-01  0000.00E+00  0000.00E+00  
218700.E-01  0000.00E+00  0000.00E+00  
221130.E-01  0000.00E+00  0000.00E+00  
223560.E-01  0000.00E+00  0000.00E+00  
225990.E-01  0000.00E+00  0000.00E+00  
228420.E-01  0000.00E+00  0000.00E+00  
230850.E-01  0000.00E+00  0000.00E+00  
233280.E-01  0000.00E+00  0000.00E+00  
235710.E-01  0000.00E+00  0000.00E+00  
238140.E-01  0000.00E+00  0000.00E+00  
240570.E-01  0000.00E+00  0000.00E+00  
243000.E-01  0000.00E+00  0000.00E+00  

 To convert to Energy Lost - multiply by Average Binding Energy =  3  eV/Vacancy
